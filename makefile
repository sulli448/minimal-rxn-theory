run:
ifdef n
ifeq ($(n),0)
	./wsaw < wsaw_projectile.inp > wsaw_projectile.out && ./wsaw < wsaw_target.inp > wsaw_target.out && ./fold < fold.inp > fold.out && ./dwhi < dwhi.inp > dwhi.out
else
	./wsaw < wsaw_projectile.inp > wsaw_projectile.out && ./wsaw < wsaw_target.inp > wsaw_target.out && ./fold < fold$(n).inp > fold$(n).out && ./dwhi < dwhi$(n).inp > dwhi$(n).out
endif
else
	./wsaw < wsaw_projectile.inp > wsaw_projectile.out && ./wsaw < wsaw_target.inp > wsaw_target.out && ./fold < fold.inp > fold.out && ./dwhi < dwhi.inp > dwhi.out
endif

full: origami run ar

gen:
ifdef input
	./origami.py < $(input)
else
	./origami.py < inputfile_log
endif


ar:
ifdef dir
	mkdir $(dir)
	mv *.inp *.out *.plot PROJ TARG FORMFAC $(dir)/
	mv inputfile_log $(dir)/inputfile

else
	mkdir result
	mv *.inp *.out *.plot PROJ TARG FORMFAC result
	mv inputfile_log $(dir)/result

endif

clean:
	rm -rf *.out
	rm -rf WRK*
	rm -rf *~
	rm -rf *.plot
	rm -rf PROJ
	rm -rf TARG
	rm -rf FORMFAC
ifdef full
	rm -rf *.inp
endif
