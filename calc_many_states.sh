#!/bin/bash

calc_many_states() {
	rm -rf temp
	mkdir temp
	for i in $(seq 0 $2);
	do
		./origami.py -target_state=$i < $1  # generate wsaw, fold, and dwhi input files
		make run n=$i                                                  # run wsaw, dwhi, and fold
		make ar dir=temp/state$i                                       # archive results for a given state into output directory
	done
	rm -rf dir_$1
	mv temp dir_$1
}
